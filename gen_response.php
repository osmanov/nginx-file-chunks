<?php

function usage() {
	$executable = $GLOBALS['argv'][0];

	echo <<<EOQ

Usage: php $executable <filename>

<filename> Path to file to split into chunks

The script outputs XML for the Nginx's file_chunks filter module.
EOQ;
	exit();
}

function print_block($d) {
	echo "
	  <block>
        <h>{$d['h']}</h>
        <s>{$d['s']}</s>
        <o>{$d['o']}</o>
        <n>{$d['n']}</n>
        <e>{$d['e']}</e>
        <fn>{$d['fn']}</fn>
      </block>\n";
}

$filename = isset($argv[1]) ? $argv[1] : NULL;
if (!$filename) {
	usage();
}

$filesize = filesize($filename);

$fp = fopen($filename, 'r');
if (!$fp) {
	fprintf(STDERR, "failed to open file '$filename' for reading\n");
	usage();
}

echo '<?xml version="1.0" encoding="UTF-8"?>', PHP_EOL;
echo "<response>\n<file>\n<_id>51e94b2e191c2b4b64000000</_id>\n";
echo "<blocks>\n";

$extent_id = '51e94b2e191c2b4b64000001';
$n_chunks = 4;
$n_chunk_size = floor($filesize / $n_chunks);

for ($s = $n_chunk_size, $o = 0, $n = 0; $n <= $n_chunks; $n++) {
	$size = ($n == $n_chunks ? ($filesize - ($n_chunks * $n_chunk_size)) : $s);
	if ($size == 0) break;

	print_block(array(
		'h' => 'hash_' .rand(),
		's' => $size,
		'o' => $o,
		'n' => $n,
		'e' => $extent_id,
		'fn' => $n,
	));

	$o += $s;
}

echo "</blocks>\n";
echo "
    <data_hash>",md5_file($filename),"</data_hash>
    <dir/>
    <info>
      <length>$filesize</length>
      <name>",htmlspecialchars(basename($filename)),"</name>
      <piece>4194304</piece>
      <pieces>V84MZMi4Q1BuK66QdqrUb8H9bHk=</pieces>
    </info>
    <info_hash>1AA141C114F0C65C8702C1CC54CC3DB028DFB90A</info_hash>
    <mime>text/plain</mime>
    <tth>VNPDScybf5V6L3ziBOVC7WlU1NiApg0+</tth>
</file>
<extents>
<extent>
  <_id>$extent_id</_id>
  <storage id=\"YzkyMzE0OTVkNmYz\">",htmlspecialchars($filename),"</storage>
</extent>
</extents>
<success>1</success>
<verifyData/>
</response>\n";

fclose($fp);
?>
