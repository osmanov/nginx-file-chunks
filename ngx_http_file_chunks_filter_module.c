/**
 * Nginx filter module fetching static files chunk-by-chunk.
 *
 * The chunks are specified in XML returned by an upstream.
 *
 * Copyright (c) 2013 Ruslan Osmanov <rrosmanov at gmail dot com>
 */

/* TODO:
 *
 *  Add file_chunks_salt configuration for hash checks
 *
 *  Add file_chunks_max_blocks configuration
 *
 */

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

#include <libxml/xmlstring.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

/* Note: xmlStrcmp and xmlStrcasecmp are rather fast.
 * So don't bother with extra string comparison optimizations.
 */
#define _NODE_IS(_node, _name) (!xmlStrcasecmp((_node)->name, (const xmlChar *) (_name)))

#define _xml_node_uint_val(_node) ((ngx_uint_t) _xml_node_offset_val(_node))

#define _xml_free_ngx_str(_s) \
    if ((_s).data) {          \
        xmlFree((_s).data);   \
        (_s).data = NULL;     \
        (_s).len = 0;         \
    }

typedef struct {
    xmlChar   *id;
    xmlChar   *storage_id;
    ngx_str_t  storage;

    void *next; /* linked list */
} ngx_http_file_chunks_xml_extent_t;

typedef struct {
    xmlChar    *h;
    off_t       s;
    ngx_uint_t  o;
    ngx_uint_t  n;
    xmlChar    *e;
    ngx_uint_t  fn;
} ngx_http_file_chunks_xml_block_t;

typedef struct {
    ngx_array_t *blocks; /* array of ngx_http_file_chunks_xml_block_t */

    ngx_http_file_chunks_xml_extent_t *_head_extent;   /* head of linked list */

    xmlChar   *data_hash;
    ngx_str_t  mime;
    xmlChar   *tth;

    off_t       length;   /* total file size                   */
    time_t      mtime;    /* max. block file modification time */
    ngx_uint_t  piece;
    ngx_str_t   name;
    xmlChar    *pieces;
} ngx_http_file_chunks_xml_info_t;

typedef struct {
    ngx_flag_t   enable;
    ngx_hash_t   input_types;
    ngx_array_t *input_type_keys;
} ngx_http_file_chunks_conf_t;

typedef struct {
    ngx_uint_t          done;      /* unsigned:1                                           */
    ngx_http_request_t *request;
    xmlDocPtr           doc;
    xmlParserCtxtPtr    ctxt;      /* XML parser context for using the parser in push mode */
    ngx_http_file_chunks_xml_info_t *xml;
} ngx_http_file_chunks_ctx_t;


static ngx_int_t ngx_http_file_chunks_filter_init(ngx_conf_t *cf);
static ngx_int_t ngx_http_file_chunks_filter_preconfigure(ngx_conf_t *cf);
static void ngx_http_file_chunks_filter_exit(ngx_cycle_t *cycle);
static void *ngx_http_file_chunks_create_loc_conf(ngx_conf_t *cf);
static char *ngx_http_file_chunks_merge_loc_conf(ngx_conf_t *cf,
        void *parent, void *child);
static ngx_int_t
ngx_http_file_chunks_parse_chunk( ngx_http_file_chunks_ctx_t *ctx,
        ngx_buf_t *b);
static ngx_int_t ngx_http_file_chunks_send(ngx_http_file_chunks_ctx_t *ctx,
        ngx_int_t success);
static void ngx_http_file_chunks_cleanup(void *data);
static ngx_buf_t *
ngx_http_file_chunks_chain_file(ngx_http_file_chunks_xml_block_t *block,
        ngx_http_file_chunks_ctx_t *ctx, ngx_int_t *rc);
static ngx_int_t
ngx_http_file_chunks_fill_xml_info(ngx_http_file_chunks_ctx_t *ctx);
static void
ngx_http_file_chunks_free_xml_info(ngx_http_file_chunks_xml_info_t *xml_info);


static ngx_http_output_header_filter_pt ngx_http_next_header_filter;
static ngx_http_output_body_filter_pt   ngx_http_next_body_filter;

static const char NGX_HTTP_FILE_CHUNKS_DTD_STR[] =
"<!ELEMENT response (file,extents,success,verifyData?)>"
"<!ELEMENT file (_id,blocks,data_hash,dir?,info,info_hash,mime,tth)>"
"<!ELEMENT _id (#PCDATA)>"
"<!ELEMENT blocks (block+)>"
"<!ELEMENT block (h,s,o,n,e,fn)>"
"<!ELEMENT h (#PCDATA)>"
"<!ELEMENT s (#PCDATA)>"
"<!ELEMENT o (#PCDATA)>"
"<!ELEMENT n (#PCDATA)>"
"<!ELEMENT e (#PCDATA)>"
"<!ELEMENT fn (#PCDATA)>"
"<!ELEMENT data_hash (#PCDATA)>"
"<!ELEMENT dir (#PCDATA)>"
"<!ELEMENT info (length,name,piece,pieces)>"
"<!ELEMENT length (#PCDATA)>"
"<!ELEMENT name (#PCDATA)>"
"<!ELEMENT piece (#PCDATA)>"
"<!ELEMENT pieces (#PCDATA)>"
"<!ELEMENT info_hash (#PCDATA)>"
"<!ELEMENT mime (#PCDATA)>"
"<!ELEMENT tth (#PCDATA)>"
"<!ELEMENT extents (extent+)>"
"<!ELEMENT extent (_id,storage+)>"
"<!ELEMENT storage (#PCDATA)>"
"<!ATTLIST storage id CDATA #REQUIRED>"
"<!ELEMENT success (#PCDATA)>"
"<!ELEMENT verifyData EMPTY>";

static xmlDtdPtr ngx_http_file_chunks_dtd = NULL;

static ngx_str_t ngx_http_file_chunks_default_types[] = {
    ngx_string("text/xml"),
    ngx_string("application/xml"),
    ngx_null_string
};

static ngx_command_t ngx_http_file_chunks_filter_commands[] = {
    { ngx_string("file_chunks"),
      NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF |
          NGX_HTTP_LIF_CONF | NGX_CONF_FLAG,
      ngx_conf_set_flag_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_file_chunks_conf_t, enable),
      NULL },

    { ngx_string("file_chunks_input_types"),
      NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF
          | NGX_CONF_1MORE,
      ngx_http_types_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_file_chunks_conf_t, input_type_keys),
      &ngx_http_file_chunks_default_types[0] },

      ngx_null_command
};

static ngx_http_module_t ngx_http_file_chunks_filter_module_ctx = {
    ngx_http_file_chunks_filter_preconfigure, /* preconfiguration              */
    ngx_http_file_chunks_filter_init,         /* postconfiguration             */

    NULL,                                     /* create main configuration     */
    NULL,                                     /* init main configuration       */

    NULL,                                     /* create server configuration   */
    NULL,                                     /* merge server configuration    */

    ngx_http_file_chunks_create_loc_conf,     /* create location configuration */
    ngx_http_file_chunks_merge_loc_conf       /* merge location configuration  */
};

ngx_module_t ngx_http_file_chunks_filter_module = {
    NGX_MODULE_V1,
    &ngx_http_file_chunks_filter_module_ctx, /* module context    */
    ngx_http_file_chunks_filter_commands,    /* module directives */
    NGX_HTTP_MODULE,                         /* module type       */
    NULL,                                    /* init master       */
    NULL,                                    /* init module       */
    NULL,                                    /* init process      */
    NULL,                                    /* init thread       */
    NULL,                                    /* exit thread       */
    ngx_http_file_chunks_filter_exit,        /* exit process      */
    ngx_http_file_chunks_filter_exit,        /* exit master       */
    NGX_MODULE_V1_PADDING
};


static void _printNode(const ngx_http_request_t *r, xmlNodePtr node)
{
	if (xmlNodeIsText(node)) return;

    xmlChar *xmlstr;
	const xmlChar *name = node->name;
	if (!name) {
		return;
	}

	while (node != NULL) {
		if (xmlNodeIsText(node)) break;
		node = node->children;
	}

	if (node) {
	    xmlstr = xmlNodeGetContent(node);

        ngx_log_debug2(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks node %s: %s", name, xmlstr);

        xmlFree(xmlstr);
	}
}


static ngx_inline off_t
_xml_node_offset_val(const xmlNodePtr node)
{
    ngx_int_t res;
    xmlChar *xmlstr;
    
    xmlstr = xmlNodeGetContent(node);
    if (xmlstr == NULL) {
        return 0;
    }

    res = (ngx_uint_t) strtoul((const char *) xmlstr, NULL, 10);

    xmlFree(xmlstr);

    return res;
}



/* {{{ ngx_http_file_chunks_init_dtd */
static ngx_int_t
ngx_http_file_chunks_init_dtd(const ngx_conf_t *cf)
{
    xmlParserInputBufferPtr xml_buf;

    xml_buf = xmlAllocParserInputBuffer(XML_CHAR_ENCODING_NONE);
    if (xml_buf == NULL) {
        ngx_log_error(NGX_LOG_ERR, cf->log, 0, "xmlAllocParserInputBuffer() failed");
        return NGX_ERROR;
    }
    xmlParserInputBufferPush(xml_buf,
            sizeof(NGX_HTTP_FILE_CHUNKS_DTD_STR) - 1,
            (const char *) NGX_HTTP_FILE_CHUNKS_DTD_STR);
    if (xml_buf == NULL) {
        ngx_log_error(NGX_LOG_ERR, cf->log, 0, "xmlParserInputBufferPush() failed");
        return NGX_ERROR;
    }

    /* Note: xml_buf will be free'd by xmlIOParseDTD in any case */
    ngx_http_file_chunks_dtd = xmlIOParseDTD(NULL, xml_buf, XML_CHAR_ENCODING_NONE);
    if (ngx_http_file_chunks_dtd == NULL) {
        ngx_log_error(NGX_LOG_ERR, cf->log, 0, "xmlIOParseDTD() failed");
        return NGX_ERROR;
    }

    return NGX_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_preconfiguration */
static ngx_int_t
ngx_http_file_chunks_filter_preconfigure(ngx_conf_t *cf)
{
    ngx_int_t rc;

    ngx_log_error(NGX_LOG_NOTICE, cf->log, 0, "file_chunks preconf");

    xmlInitParser();

    rc = ngx_http_file_chunks_init_dtd(cf);
    if (rc != NGX_OK) {
        return NGX_ERROR;
    }

    return NGX_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_create_loc_conf */
static void *
ngx_http_file_chunks_create_loc_conf(ngx_conf_t *cf)
{
    ngx_log_error(NGX_LOG_NOTICE, cf->log, 0, "file_chunks create loc conf");

    ngx_http_file_chunks_conf_t *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_file_chunks_conf_t));
    if (conf == NULL) {
        return NULL;
    }

    /*
     * set by ngx_pcalloc():
     *
     *     conf->input_types = { NULL };
     *     conf->input_type_keys = NULL;
     */

    conf->enable = NGX_CONF_UNSET;

    return conf;
}
/* }}} */


/* {{{ ngx_http_file_chunks_merge_loc_conf */
static char *
ngx_http_file_chunks_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_file_chunks_conf_t *prev = parent;
    ngx_http_file_chunks_conf_t *conf = child;

    ngx_conf_merge_value(conf->enable, prev->enable, 0);

    if (ngx_http_merge_types(cf, &conf->input_type_keys, &conf->input_types,
                &prev->input_type_keys, &prev->input_types,
                ngx_http_file_chunks_default_types) != NGX_OK) {
        return NGX_CONF_ERROR;
    }

    return NGX_CONF_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_header_filter */
static ngx_int_t
ngx_http_file_chunks_header_filter(ngx_http_request_t *r)
{
    ngx_http_file_chunks_ctx_t  *ctx;
    ngx_http_file_chunks_conf_t *conf;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0, "file_chunks header filter");

    if (r->headers_out.status == NGX_HTTP_NOT_MODIFIED
            || (r->headers_out.status != NGX_HTTP_OK
                && r->headers_out.status != NGX_HTTP_FORBIDDEN
                && r->headers_out.status != NGX_HTTP_NOT_FOUND)
            || r->header_only) {
        return ngx_http_next_header_filter(r);
    }

    conf = ngx_http_get_module_loc_conf(r, ngx_http_file_chunks_filter_module);

    if (!conf->enable
            || ngx_http_test_content_type(r, &conf->input_types) == NULL) {
        return ngx_http_next_header_filter(r);
    }

    ctx = ngx_http_get_module_ctx(r, ngx_http_file_chunks_filter_module);
    if (ctx) {
        return ngx_http_next_header_filter(r);
    }

    ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_file_chunks_ctx_t));
    if (ctx == NULL) {
        return NGX_ERROR;
    }

    ngx_http_set_ctx(r, ctx, ngx_http_file_chunks_filter_module);

    ctx->request = r;

    /* Set content type to text/html for the case of error pages */
    /*
    r->headers_out.content_type_len = sizeof("text/html") - 1;
    ngx_str_set(&r->headers_out.content_type, "text/html");
    */

    r->headers_out.content_type.len = 0;
    if (ngx_http_set_content_type(r) != NGX_OK) {
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    r->main_filter_need_in_memory = 1;
    
    ngx_http_clear_content_length(r);
    ngx_http_clear_accept_ranges(r);
    ngx_http_clear_etag(r);

    return ngx_http_next_header_filter(r);
}/* }}} */


static void
_dtd_validity_error(void *data, const char *msg, ...)
{
    ngx_http_file_chunks_ctx_t *ctx;
    xmlParserCtxtPtr            ctxt;
    size_t                      n;
    va_list                     args;
    u_char                      buf[NGX_MAX_ERROR_STR];

    ctxt = data;

    ctx = ctxt->sax->_private;

    buf[0] = '\0';

    va_start(args, msg);
    n = (size_t) vsnprintf((char *) buf, NGX_MAX_ERROR_STR, msg, args);
    va_end(args);

    while (--n && (buf[n] == CR || buf[n] == LF)) { /* void */ }

    ngx_log_error(NGX_LOG_ERR, ctx->request->connection->log, 0,
                  "libxml2 error: \"%*s\"", n + 1, buf);
}

void
_dtd_validity_warning(void *data, const char *msg, ...)
{
    ngx_http_file_chunks_ctx_t *ctx;
    xmlParserCtxtPtr            ctxt;
    size_t                      n;
    va_list                     args;
    u_char                      buf[NGX_MAX_ERROR_STR];

    ctxt = data;

    ctx = ctxt->sax->_private;

    buf[0] = '\0';

    va_start(args, msg);
    n = (size_t) vsnprintf((char *) buf, NGX_MAX_ERROR_STR, msg, args);
    va_end(args);

    while (--n && (buf[n] == CR || buf[n] == LF)) { /* void */ }

    ngx_log_error(NGX_LOG_ALERT, ctx->request->connection->log, 0,
                  "libxml2 warning: \"%*s\"", n + 1, buf);
}


/* {{{ ngx_http_file_chunks_body_filter */
static ngx_int_t
ngx_http_file_chunks_body_filter(ngx_http_request_t *r, ngx_chain_t *in)
{
    int                         well_formed;
    ngx_chain_t                *cl;
    ngx_http_file_chunks_ctx_t *ctx;
    xmlValidCtxt                vctxt;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0, "file_chunks body filter");

    if (in == NULL) {
        ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks called next body filter");
        return ngx_http_next_body_filter(r, in);
    }

    ctx = ngx_http_get_module_ctx(r, ngx_http_file_chunks_filter_module);
    if (ctx == NULL || ctx->done || r->header_only) {
        return ngx_http_next_body_filter(r, in);
    }

	vctxt.error = _dtd_validity_error;
	vctxt.warning = _dtd_validity_warning;

    for (cl = in; cl; cl = cl->next) {
        if (ngx_http_file_chunks_parse_chunk(ctx, cl->buf) != NGX_OK) {
            ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                    "file_chunks: ngx_http_file_chunks_parse_chunk failed");

            if (ctx->ctxt->myDoc) {
                ctx->ctxt->myDoc->extSubset = NULL;
                xmlFreeDoc(ctx->ctxt->myDoc);
                ctx->ctxt->myDoc = NULL;
            }

            xmlFreeParserCtxt(ctx->ctxt);
            ctx->ctxt = NULL;

            return ngx_http_file_chunks_send(ctx, 0);
        }

        /* Last chunk? */
        if (cl->buf->last_buf || cl->buf->last_in_chain) {
            well_formed = ctx->ctxt->wellFormed;
            ctx->doc = ctx->ctxt->myDoc;
            ctx->doc->extSubset = NULL;

            if (well_formed) {
                ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                        "file_chunks: last xml chunk, xml is well-formed");

                if (xmlValidateDtd(&vctxt, ctx->doc,
                            ngx_http_file_chunks_dtd) == 0) {
                    if (ctx->ctxt) {
                        xmlFreeParserCtxt(ctx->ctxt);
                        ctx->ctxt = NULL;
                    }

		            xmlFreeDoc(ctx->doc);
                    ctx->doc = NULL;

                    ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                            "file_chunks: DTD validation failed ");
    	            return NGX_ERROR;
                }

	            if (ctx->ctxt->valid == 0) {
                    if (ctx->ctxt) {
                        xmlFreeParserCtxt(ctx->ctxt);
                        ctx->ctxt = NULL;
                    }

		            xmlFreeDoc(ctx->doc);
                    ctx->doc = NULL;

                    ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                            "file_chunks: document is not valid");
    	            return NGX_ERROR;
	            }

                ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                        "file_chunks: DTD validation OK");

                return ngx_http_file_chunks_send(ctx, 1);
            }

            if (ctx->doc) {
                xmlFreeDoc(ctx->doc);
                ctx->doc = NULL;
            }

            ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                    "file_chunks: not well-formed XML");

            return ngx_http_file_chunks_send(ctx, 0);
        }
    }

    return NGX_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_filter_init */
static ngx_int_t
ngx_http_file_chunks_filter_init(ngx_conf_t *cf)
{
    ngx_log_error(NGX_LOG_NOTICE, cf->log, 0, "file_chunks init");

    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_file_chunks_header_filter;

    ngx_http_next_body_filter = ngx_http_top_body_filter;
    ngx_http_top_body_filter = ngx_http_file_chunks_body_filter;

    return NGX_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_sax_external_subset */
static void
ngx_http_file_chunks_sax_external_subset(void *data, const xmlChar *name,
    const xmlChar *externalId, const xmlChar *systemId)
{
    xmlParserCtxtPtr ctxt = data;

    xmlDocPtr                   doc;
    ngx_http_request_t         *r;
    ngx_http_file_chunks_ctx_t *ctx;

    ctx = ctxt->sax->_private;
    r = ctx->request;

    ngx_log_debug3(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                   "file_chunks filter sax external subset: \"%s\" \"%s\" \"%s\"",
                   name       ? name       : (xmlChar *) "",
                   externalId ? externalId : (xmlChar *) "",
                   systemId   ? systemId   : (xmlChar *) "");

    doc = ctxt->myDoc;
    doc->extSubset = ngx_http_file_chunks_dtd;
}
/* }}} */


/* {{{ ngx_http_file_chunks_sax_error */
static void ngx_cdecl
ngx_http_file_chunks_sax_error(void *data, const char *msg, ...)
{
    xmlParserCtxtPtr ctxt = data;

    size_t                      n;
    va_list                     args;
    ngx_http_file_chunks_ctx_t *ctx;
    u_char                      buf[NGX_MAX_ERROR_STR];

    ctx = ctxt->sax->_private;

    buf[0] = '\0';

    va_start(args, msg);
    n = (size_t) vsnprintf((char *) buf, NGX_MAX_ERROR_STR, msg, args);
    va_end(args);

    while (--n && (buf[n] == CR || buf[n] == LF)) { /* void */ }

    ngx_log_error(NGX_LOG_ERR, ctx->request->connection->log, 0,
                  "libxml2 error: \"%*s\"", n + 1, buf);
}
/* }}} */


/* {{{ ngx_http_file_chunks_parse_chunk */
static ngx_int_t
ngx_http_file_chunks_parse_chunk(ngx_http_file_chunks_ctx_t *ctx, ngx_buf_t *b)
{
    int                 err;
    xmlParserCtxtPtr    ctxt;
    ngx_http_request_t *r    = ctx->request;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "ngx_http_file_chunks_parse_chunk");

    if (ctx->ctxt == NULL) {
        ctxt = xmlCreatePushParserCtxt(NULL, NULL, NULL, 0, NULL);
        if (ctxt == NULL) {
            ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                    "xmlCreatePushParserCtxt() failed");
            return NGX_ERROR;
        }
        ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks: created parser ctxt");
        xmlCtxtUseOptions(ctxt, XML_PARSE_NOENT | XML_PARSE_DTDLOAD
                | XML_PARSE_NOWARNING);

        ctxt->sax->externalSubset     = ngx_http_file_chunks_sax_external_subset;
        ctxt->sax->setDocumentLocator = NULL;
        ctxt->sax->error              = ngx_http_file_chunks_sax_error;
        ctxt->sax->fatalError         = ngx_http_file_chunks_sax_error;
        ctxt->sax->_private           = ctx;

        ctx->ctxt    = ctxt;
        ctx->request = r;
    }

    err = xmlParseChunk(ctx->ctxt, (char *) b->pos, (int) (b->last - b->pos),
            (b->last_buf || b->last_in_chain));
    if (err == 0) {
        ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks: read last xml chunk");
        b->pos = b->last;
        return NGX_OK;
    }

    ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
            "xmlParseChunk() failed, error: %d", err);

    return NGX_ERROR;
}
/* }}} */


/* {{{ ngx_http_file_chunks_cleanup */
static void
ngx_http_file_chunks_cleanup(void *data)
{
    if (data) {
        ngx_free(data);
    }
}
/* }}} */


/* {{{ ngx_http_file_chunks_cleanup_dtd */
static void
ngx_http_file_chunks_cleanup_dtd(void *data)
{
    xmlFreeDtd(data);
}
/* }}} */


/* {{{ ngx_http_file_chunks_send */
static ngx_int_t
ngx_http_file_chunks_send(ngx_http_file_chunks_ctx_t *ctx, ngx_int_t success)
{
    ngx_buf_t                         *b, *b_last = NULL;
    ngx_int_t                          rc;
    ngx_uint_t                         i;
    ngx_chain_t                      **cl_last_out  , *cl, out;
    ngx_http_file_chunks_xml_block_t  *block_elts   , *block;
    ngx_http_file_chunks_xml_info_t    xml_info;
    ngx_http_request_t                *r;
    off_t                              length;

    r = ctx->request;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: sending files");

    ctx->done = 1;

    if (success == 0) {
        return ngx_http_filter_finalize_request(r,
                &ngx_http_file_chunks_filter_module,
                NGX_HTTP_INTERNAL_SERVER_ERROR);
    }

    rc = ngx_http_discard_request_body(r);
    if (rc != NGX_OK) {
        return rc;
    }

#if 0
    rc = ngx_http_next_header_filter(r);
    if (rc == NGX_ERROR || rc > NGX_OK || r->header_only) {
        ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks: header filter failed");
        return rc;
    }
#endif


    ctx->xml = &xml_info;
    rc = ngx_http_file_chunks_fill_xml_info(ctx);
    if (rc != NGX_OK) {
        return rc;
    }

    cl_last_out = NULL;

    i = 0;
    length = 0;
    block_elts = xml_info.blocks->elts;

    while (i < xml_info.blocks->nelts) {
        block = &(block_elts[i]);

        b = ngx_http_file_chunks_chain_file(block, ctx, (ngx_int_t *) &rc);
        if (b == NULL || rc != NGX_OK) {
            break;
        }

        b_last = b;

        if (cl_last_out == NULL) {
            out.buf = b;
            cl_last_out = &out.next;
            out.next = NULL;
        } else {
            cl = ngx_alloc_chain_link(r->pool);
            if (cl == NULL) {
                return NGX_HTTP_INTERNAL_SERVER_ERROR;
            }

            cl->buf = b;

            *cl_last_out = cl;
            cl_last_out  = &cl->next;
            cl->next     = NULL;
        }

        length += block->s;

        i++;
    }

    if (b_last != NULL) {
        b_last->last_in_chain = 1;
        b_last->last_buf = 1;
    }


    /* File length should be equal to the total length collected from the file
     * chunk blocks */
    if (xml_info.length != length) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                "file_chunks: total file size != sum of block sizes");
        rc = NGX_HTTP_BAD_REQUEST;
        goto err;
    }

    if (rc != NGX_OK) {
        return rc;
    }
    
    /* XXX ???
     * Content-disposition: filename={xml_info.name};length={xml_info.length} */
    r->headers_out.content_type.len = 0;
    if (ngx_http_set_content_type(r) != NGX_OK) {
        rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto err;
    }
    
    r->headers_out.status             = NGX_HTTP_OK;
    r->headers_out.content_length_n   = xml_info.length;
    r->headers_out.last_modified_time = xml_info.mtime;

    ngx_log_debug3(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: (xml) mime: %V, len: %d, cont len: %d",
            &xml_info.mime, (int) xml_info.mime.len, xml_info.length);
    if (xml_info.mime.len) {
        r->headers_out.content_type_len = xml_info.mime.len;
        r->headers_out.content_type.len = xml_info.mime.len;
        r->headers_out.content_type.data = xml_info.mime.data;
    } else {
        r->headers_out.content_type_len = sizeof("application/octet-stream") - 1;
        ngx_str_set(&r->headers_out.content_type, "application/octet-stream");
    }
    r->headers_out.content_type_lowcase = NULL;

    if (ngx_http_set_etag(r) != NGX_OK) {
        rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto err;
    }

    if (ngx_http_set_content_type(r) != NGX_OK) {
        rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto err;
    }

    r->headers_out.content_length_n   = xml_info.length;
    rc = ngx_http_send_header(r);
    if (rc == NGX_ERROR || rc > NGX_OK || r->header_only) {
        goto err;
    }

ret:
    ngx_http_file_chunks_free_xml_info(ctx->xml);

    return ngx_http_next_body_filter(r, &out);

err:
    ngx_http_file_chunks_free_xml_info(ctx->xml);

    return rc;
}
/* }}} */


/* {{{ ngx_http_file_chunks_parse_xml_block */
static ngx_inline void
ngx_http_file_chunks_parse_xml_block(xmlNodePtr node,
        ngx_http_file_chunks_ctx_t *ctx)
{
    ngx_http_file_chunks_xml_info_t  *xml_info = ctx->xml;
    ngx_http_file_chunks_xml_block_t *block;
    const ngx_http_request_t         *r        = ctx->request;

    if (!_NODE_IS(node, "block")) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, ngx_errno,
                "file_chunks: expected block node, got '%s'", node->name);
        return;
    }

    node = node->children;
    if (node == NULL) {
        return;
    }

    block = ngx_array_push(xml_info->blocks);
    if (block == NULL) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, ngx_errno,
                "file_chunks failed to allocate xml block in memory");
        return;
    }
    ngx_memzero(block, sizeof(ngx_http_file_chunks_xml_block_t));

    while (node != NULL) {
        _printNode(r, node);

        if (_NODE_IS(node, "h")) {
            block->h = xmlNodeGetContent(node);
        } else if (_NODE_IS(node, "s")) {
            block->s = _xml_node_offset_val(node);
        } else if (_NODE_IS(node, "o")) {
            block->o = _xml_node_offset_val(node);
        } else if (_NODE_IS(node, "n")) {
            block->n = _xml_node_uint_val(node);
        } else if (_NODE_IS(node, "e")) {
            block->e = xmlNodeGetContent(node);
        } else if (_NODE_IS(node, "fn")) {
            block->fn = _xml_node_uint_val(node);
        }

        node = node->next;
    }

    if (block->h == NULL || block->e == NULL) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, ngx_errno,
                "file_chunks: invalid block with empty h or e");
    }
}
/* }}} */


/* {{{ ngx_http_file_chunks_parse_xml_extent */
static ngx_inline void
ngx_http_file_chunks_parse_xml_extent(xmlNodePtr node,
        ngx_http_file_chunks_ctx_t *ctx)
{
    xmlChar                           *xmlstr;
    ngx_http_file_chunks_xml_info_t   *xml;
    ngx_http_file_chunks_xml_extent_t *e;
    ngx_http_request_t                *r;
    
    xml = ctx->xml;
    r   = ctx->request;

    node = node->children;
    if (node == NULL) {
        return;
    }

    e = ngx_pcalloc(r->pool, sizeof(ngx_http_file_chunks_xml_extent_t));
    if (e == NULL) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                "file_chunks: failed to allocate memory for extent");
        return;
    }
    if (xml->_head_extent) {
        e->next = xml->_head_extent;
    }
    xml->_head_extent = e;

    while (node != NULL) {
        _printNode(r, node);

        if (_NODE_IS(node, "_id")) {
            e->id = xmlNodeGetContent(node);
        } else if (_NODE_IS(node, "storage")) {
            xmlstr = xmlNodeGetContent(node);
            e->storage.data = xmlstr;
            e->storage.len = xmlStrlen(xmlstr);

            e->storage_id = xmlGetProp(node, "id");
        }

        node = node->next;
    }
}
/* }}} */



/* {{{ ngx_http_file_chunks_parse_xml
 * Parses node and it's children recursively, and fills ctx->xml */
static void
ngx_http_file_chunks_parse_xml(xmlNodePtr node,
        ngx_http_file_chunks_ctx_t *ctx)
{
    xmlChar                         *xmlstr;
    ngx_http_file_chunks_xml_info_t *xml_info = ctx->xml;
    ngx_http_request_t              *r        = ctx->request;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: ngx_http_file_chunks_parse_xml");

    node = node->children;

    while (node != NULL) {
        if (xmlNodeIsText(node)) {
            node = node->next;
            continue;
        }

        _printNode(r, node);

        if (node->children) {
            if (_NODE_IS(node, "block")) {
                ngx_http_file_chunks_parse_xml_block(node, ctx);
                goto next;
            } else if (_NODE_IS(node, "extent")) {
                ngx_http_file_chunks_parse_xml_extent(node, ctx);
                goto next;
            }

            ngx_http_file_chunks_parse_xml(node, ctx);
        }

        if (_NODE_IS(node, "data_hash")) {
            xml_info->data_hash = xmlNodeGetContent(node);
        } else if (_NODE_IS(node, "mime")) {
            xmlstr = xmlNodeGetContent(node);
            xml_info->mime.data = xmlstr;
            xml_info->mime.len = xmlStrlen(xmlstr);

            ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                    "file_chunks: (xml) mime: %V",
                    &xml_info->mime);
        } else if (_NODE_IS(node, "tth")) {
            xml_info->tth = xmlNodeGetContent(node);
        } else if (_NODE_IS(node, "length")) {
            xml_info->length = _xml_node_offset_val(node);
        } else if (_NODE_IS(node, "piece")) {
            xml_info->piece = _xml_node_uint_val(node);
        } else if (_NODE_IS(node, "name")) {
            xmlstr = xmlNodeGetContent(node);
            xml_info->name.data = xmlstr;
            xml_info->name.len = xmlStrlen(xmlstr);
        } else if (_NODE_IS(node, "pieces")) {
            xml_info->pieces = xmlNodeGetContent(node);
        }

next:
        node = node->next;
    }
}
/* }}} */


/* {{{ ngx_http_file_chunks_sort_xml_blocks */
static int ngx_libc_cdecl
ngx_http_file_chunks_sort_xml_blocks(const void *b1, const void *b2)
{
    ngx_http_file_chunks_xml_block_t *block1, *block2;

    block1 = (ngx_http_file_chunks_xml_block_t *) b1;
    block2 = (ngx_http_file_chunks_xml_block_t *) b2;

    return (block1->fn - block2->fn);
}
/* }}} */


/* {{{ ngx_http_file_chunks_fill_xml_info */
static ngx_int_t
ngx_http_file_chunks_fill_xml_info(ngx_http_file_chunks_ctx_t *ctx)
{
    xmlNodePtr                       root_node;
    ngx_http_file_chunks_xml_info_t *xml_info = ctx->xml;
    ngx_http_request_t              *r        = ctx->request;


    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: filling xml info");

    root_node = xmlDocGetRootElement(ctx->doc);
    if (root_node == NULL) {
        ngx_log_error(NGX_LOG_ALERT, r->connection->log, 0,
                "file_chunks: empty XML document");
        return NGX_ERROR;
    }

    ngx_memzero(xml_info, sizeof(ngx_http_file_chunks_xml_info_t));

    xml_info->blocks = ngx_array_create(r->pool, 8,
            sizeof(ngx_http_file_chunks_xml_block_t));
    if (xml_info->blocks == NULL) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                "file_chunks: failed to allocate xml info blocks array");
        return NGX_ERROR;
    }

    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: (1) num blocks: %d", xml_info->blocks->nelts);

    ngx_http_file_chunks_parse_xml(root_node, ctx);

    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: (2) num blocks: %d", xml_info->blocks->nelts);

    ngx_qsort(xml_info->blocks->elts,
            (size_t) xml_info->blocks->nelts,
            sizeof(ngx_http_file_chunks_xml_block_t),
            ngx_http_file_chunks_sort_xml_blocks);

    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: (3) num blocks: %d", xml_info->blocks->nelts);

    return NGX_OK;
}
/* }}} */


/* {{{ ngx_http_file_chunks_get_extent_filename */
static ngx_int_t
ngx_http_file_chunks_get_extent_filename(ngx_str_t *out_filename,
        const ngx_http_file_chunks_xml_block_t *block,
        const ngx_http_file_chunks_ctx_t *ctx)
{
    ngx_http_file_chunks_xml_extent_t *e;
    xmlChar                           *extent_id = block->e;

    e = ctx->xml->_head_extent;
    while (e != NULL) {
        if (xmlStrcasecmp(e->id, extent_id) == 0) {
            *out_filename = e->storage;
            return NGX_OK;
        }

        e = e->next;
    }

    return NGX_HTTP_NOT_FOUND;
}
/* }}} */

/* {{{ ngx_http_file_chunks_chain_file */
static ngx_buf_t *
ngx_http_file_chunks_chain_file(ngx_http_file_chunks_xml_block_t *block,
        ngx_http_file_chunks_ctx_t *ctx, ngx_int_t *rc)
{
    ngx_buf_t                        *b;
    ngx_http_file_chunks_xml_info_t  *xml_info;
    ngx_open_file_info_t              of;
    ngx_http_core_loc_conf_t         *ccf;
    ngx_str_t                         filename;
    ngx_uint_t                        i         , err_level;
    ngx_chain_t                      *cl;
    ngx_http_request_t               *r;

    r = ctx->request;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "ngx_http_file_chunks_chain_file");
    
    xml_info = ctx->xml;
    if (!xml_info) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0,
                "file_chunks: failed to chain file, empty XML info");
        *rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto error;
    }

    ccf = ngx_http_get_module_loc_conf(r, ngx_http_core_module);

    ngx_memzero(&of, sizeof(ngx_open_file_info_t));

    of.read_ahead = ccf->read_ahead;
    of.directio   = ccf->directio;
    of.valid      = ccf->open_file_cache_valid;
    of.min_uses   = ccf->open_file_cache_min_uses;
    of.errors     = ccf->open_file_cache_errors;
    of.events     = ccf->open_file_cache_events;

    *rc = ngx_http_file_chunks_get_extent_filename(&filename, block, ctx);
    if (*rc != NGX_OK) {
        ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                "file_chunks: no extent found for block fn:%d",
                block->fn);
        goto error;
    }
    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks filename: '%V'", &filename);

    if (ngx_open_cached_file(ccf->open_file_cache, &filename, &of, r->pool)
            != NGX_OK) {
        switch (of.err) {
            case 0:
                *rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
                goto error;

            case NGX_ENOENT:
            case NGX_ENOTDIR:
            case NGX_ENAMETOOLONG:
                err_level = NGX_LOG_ERR;
                *rc = NGX_HTTP_NOT_FOUND;
                break;

            case NGX_EACCES:
                err_level = NGX_LOG_ERR;
                *rc = NGX_HTTP_FORBIDDEN;
                break;

            default:
                err_level = NGX_LOG_CRIT;
                *rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
                break;
        }

        if (*rc != NGX_HTTP_NOT_FOUND || ccf->log_not_found) {
            ngx_log_error(err_level, r->connection->log, of.err,
                    "%s \"%V\" failed", of.failed, &filename);
        }

        goto error;
    }

    if (!of.is_file) {
        ngx_log_error(NGX_LOG_CRIT, r->connection->log, ngx_errno,
                "\"%V\" is not a regular file", &filename);
        *rc = NGX_HTTP_NOT_FOUND;
        goto error;
    }

    if (of.size == 0) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, ngx_errno,
                "file_chunks block \"%V\" is empty", &filename);
        *rc = NGX_HTTP_BAD_REQUEST;
        goto error;
    }

    if (of.mtime > xml_info->mtime) {
        xml_info->mtime = of.mtime;
    }

    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));
    if (b == NULL) {
        *rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto error;
    }

    b->file = ngx_pcalloc(r->pool, sizeof(ngx_file_t));
    if (b->file == NULL) {
        *rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
        goto error;
    }
    ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
            "file_chunks: fd:%d",
            of.fd);

    b->file_pos       = block->o;
    b->file_last      = of.size;
    b->file->fd       = of.fd;
    b->file->directio = of.is_directio;
    b->in_file        = b->file_last ? 1 : 0;
    b->file->name     = filename;
    b->file->log      = r->connection->log;

    *rc = NGX_OK;

    return b;

error:
    return NULL;
}
/* }}} */


/* {{{ ngx_http_file_chunks_filter_exit */
static void
ngx_http_file_chunks_filter_exit(ngx_cycle_t *cycle)
{
    xmlCleanupParser();
}
/* }}} */


static ngx_inline void
ngx_http_file_chunks_free_xml_extent(ngx_http_file_chunks_xml_extent_t *extent)
{
    /* We allocated this separately, nginx knows nothing about it */
    if (extent->storage_id) {
        xmlFree(extent->storage_id);
        extent->storage_id = NULL;
    }

    _xml_free_ngx_str(extent->storage);

#if 0
    /* We catch double free here, since Nginx itself takes care of freeing the
     * palloc'd/pmalloc'd resources */
    ngx_free(extent);
#endif
}


static ngx_inline void
ngx_http_file_chunks_free_xml_block(ngx_http_file_chunks_xml_block_t *b)
{
    if (b->h) {
        xmlFree(b->h);
        b->h = NULL;
    }

    if (b->e) {
        xmlFree(b->e);
        b->e = NULL;
    }
}


/* {{{ ngx_http_file_chunks_free_xml_info */
static void
ngx_http_file_chunks_free_xml_info(ngx_http_file_chunks_xml_info_t *xml_info)
{
    ngx_uint_t                         i;
    ngx_http_file_chunks_xml_block_t  *block_elts  , *block;
    ngx_http_file_chunks_xml_extent_t *extent      , *tmp_extent;

    if (xml_info->data_hash) {
        xmlFree(xml_info->data_hash);
        xml_info->data_hash = NULL;
    }

    if (xml_info->pieces) {
        xmlFree(xml_info->pieces);
        xml_info->pieces = NULL;
    }

    _xml_free_ngx_str(xml_info->name);

    extent = xml_info->_head_extent;
    while (extent != NULL) {
        tmp_extent = extent->next;
        ngx_http_file_chunks_free_xml_extent(extent);
        extent = tmp_extent;
    }

    if (xml_info->blocks) {
        
    }
    i = 0;
    block_elts = xml_info->blocks->elts;

    while (i < xml_info->blocks->nelts) {
        block = &(block_elts[i]);
        ngx_http_file_chunks_free_xml_block(block);
        i++;
    }
}
/* }}} */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim: et sw=4 ts=4 sts=4 fdm=marker tw=80 path+=/usr/include/libxml2
 */
