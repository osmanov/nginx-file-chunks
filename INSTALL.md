# Installation of the `file_chunks` filter module for Nginx

To insall the `file_chunks` module, rebuild Nginx with `--add-module` option. The option should point to the directory
with the sources of `file_chunks`, e.g.:

    $ cd ~/src/nginx
    $ ./auto/configure --prefix=/home/ruslan \
        --with-debug \
        --conf-path=/home/ruslan/etc/nginx/nginx.conf \
        --user=ruslan \
        --group=www \
        --pid-path=/home/ruslan/var/run/nginx.pid \
        --lock-path=/home/ruslan/var/run/nginx.lock \
        --error-log-path=/home/ruslan/var/log/nginx/error.log \
        --add-module=/home/ruslan/projects/nginx/modules/file_chunks_filter
        

# vim: ft=markdown et
